/**
 * 2、前台服务
 * 这种大部分人都了解，据说这个微信也用过的进程保活方案，移步微信Android客户端后台保活经验分享，这方案实际利用了Android前台service的漏洞。
 * 原理如下
 * 对于 API level < 18 ：调用startForeground(ID， new Notification())，发送空的Notification ，图标则不会显示。
 * 对于 API level >= 18：在需要提优先级的service A启动一个InnerService，两个服务同时startForeground，且绑定同样的 ID。Stop 掉InnerService ，这样通知栏图标即被移除。
 * <p>
 * <p>
 * <p>
 * <p>
 * Auth dongfang
 * Date 2018/4/10
 */
package com.df.pro.alive.way2;