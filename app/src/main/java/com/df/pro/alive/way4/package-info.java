/**
 * JobSheduler是作为进程死后复活的一种手段，native进程方式最大缺点是费电，
 * Native 进程费电的原因是感知主进程是否存活有两种实现方式，在 Native 进程中通过死循环或定时器，
 * 轮训判断主进程是否存活，当主进程不存活时进行拉活。其次5.0以上系统不支持。
 * 但是JobSheduler可以替代在Android5.0以上native进程方式，这种方式即使用户强制关闭，也能被拉起来。
 * <p>
 * <p>
 * <p>
 * <p>
 * Auth dongfang
 * Date 2018/4/11
 */
package com.df.pro.alive.way4;


//需要注意的是这个job service运行在你的主线程，这意味着你需要使用子线程，handler, 或者一个异步任务来运行耗时的操作以防止阻塞主线程。
// 因为多线程技术已经超出了我们这篇文章的范围，让我们简单实现一个Handlder来执行我们在JobSchedulerService定义的任务吧。


