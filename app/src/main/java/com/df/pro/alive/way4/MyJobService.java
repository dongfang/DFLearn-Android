package com.df.pro.alive.way4;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import java.util.Objects;

/**
 * @author dongfang
 * @date 2018/4/9
 */


@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class MyJobService extends JobService {


    @Override
    public void onCreate() {
        super.onCreate();
        startJobSheduler();
    }

    public void startJobSheduler() {
        try {
            JobInfo.Builder builder = new JobInfo.Builder(1, new ComponentName(getPackageName(), MyJobService.class.getName()));
            builder.setPeriodic(5000); // 这来单位是毫秒，5000就是5秒执行一次
            builder.setPersisted(true); // 这个方法告诉系统当你的设备重启之后你的任务是否还要继续执行。
            builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // 非蜂窝网络
            builder.setRequiresCharging(false); // 是否是在充电模式下
            builder.setRequiresDeviceIdle(false); // 是否处于闲置状态


            JobScheduler jobScheduler = (JobScheduler) this.getSystemService(Context.JOB_SCHEDULER_SERVICE);
            if ((Objects.requireNonNull(jobScheduler).schedule(builder.build())) < 1) {
                Log.w("MyJobService", "这个时候说明了jobScheduler失败");
                Log.w("MyJobService", "这个时候说明了jobScheduler失败");
                Log.w("MyJobService", "这个时候说明了jobScheduler失败");
                Log.w("MyJobService", "这个时候说明了jobScheduler失败");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        // 需要在这里判断进程是否或者，如果挂了就需要拉起来

        return false;
        // 如果返回值是false,系统假设这个方法返回时任务已经执行完毕。
        // 如果返回值是true,那么系统假定这个任务正要被执行，执行任务的重担就落在了你的肩上。
        // 当任务执行完毕时你需要调用jobFinished(JobParameters params, boolean needsRescheduled)来通知系统。
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }
}
