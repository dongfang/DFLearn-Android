package com.df.pro.alive;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.df.pro.alive.processalive.R;
import com.df.pro.alive.way1.ScreenBroadcastListener;
import com.df.pro.alive.way1.ScreenManager;
import com.df.pro.alive.way2.KeepLiveService;
import com.df.pro.alive.way4.MyJobService;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // ----- 方案1： 1个像素的Activity
        final ScreenManager screenManager = ScreenManager.getInstance(MainActivity.this);
        ScreenBroadcastListener listener = new ScreenBroadcastListener(this);
        listener.registerListener(new ScreenBroadcastListener.ScreenStateListener() {
            @Override
            public void onScreenOn() {
                screenManager.finishActivity();
            }

            @Override
            public void onScreenOff() {
                screenManager.startActivity();
            }
        });

        // ---- 方案2：前台服务，notification弄成不可见
        startService(new Intent(this, KeepLiveService.class));

        // ---- 方案3： 账号同步，据说这个才是最有用的，不过不同机型好像需要适配，比如华为，小米定制了framework

        // ---- 方案4： jobScheduler,android5.0以上版本方案
        // startService(new Intent(this, MyJobService.class)); // 因为没有实现拉起进程的操作，所以也不用启动

        // ---- 方案5： 两个进程相互唤起，

        // ---- 方案6： 系统服务进程，白名单

        // ---- 方案7： 据说可以在后台播放无声的音乐，这个也只能保证在后台是不会被杀掉

        // 程序员能用的也就方案3和4能重新唤醒；
        //
        // 商务能用方案6；

        // 剩下的1，2，5 方案在内存紧张的时候会被系统自动杀掉的概率会降低很多，不过在5.0以上的版本也会失效；
        // 手动杀死应用的时候，方案1，2，5都会失效，
        //
        // 两者 原因如下

        // 因为系统5.0以上高版本手机上，杀死进程的时候不再是单单更具pid来杀了
        // Process.killProcessQuiet(pid); 5.0之前

        // Process.killProcessQuiet(app.pid);
        // Process.killProcessGroup(app.info.uid, app.pid);

        // ActivityManagerService 不仅把主进程给杀死，另外把主进程所属的进程组一并杀死，
        // 这样一来，由于子进程和主进程在同一进程组，子进程在做的事情，也就停止了。



    }

}
